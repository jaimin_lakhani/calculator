/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package calculator;

/**
 *
 * @author jaiminlakhani
 */
public class Add extends Calculator {
    
    public Add(double x, double y){
    super(x,y);
}
    
    public double add() {
        return x + y;
    }
    
}
