/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package calculator;

/**
 *
 * @author jaiminlakhani
 */
public abstract class Calculator {
    protected double x;
    protected double y;
    
    public Calculator(double x,double y) {
        this.x = x;
        this.y = y;
    }
    
}
